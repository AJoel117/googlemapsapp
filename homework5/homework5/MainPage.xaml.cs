﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;
using Xamarin.Forms.Maps;
using Xamarin.Essentials;
using System.ComponentModel;

//Joel Antony
//CS 481
//HW5
//Sources Used: Youtube, Powerpoint slides, Xamarin Docs, Google Maps API
namespace homework5
{
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.13075, -117.1605), Distance.FromMiles(1)));

            PutPins();
        }

        //Pins are created here
        private void PutPins()
        {

            var PalomarHospital = new Pin
            {
                Type = PinType.Place,
                Position = new Position(33.12180, -117.12188),
                Label = "Palomar Hospital",
                Address = "2185 Citracado Parkway, Escondido, CA 92029, USA"
            };

            var EscondidoCycleCenter = new Pin
            {
                Type = PinType.Place,
                Position = new Position(33.12834, -117.10952),
                Label = "Escondido Cycle Center",
                Address = "1415 Montiel Rd, Escondido, CA 92026, USA"
            };
            var OceansidePier = new Pin
            {
                Type = PinType.Place,
                Position = new Position(33.19298, -117.38661),
                Label = "Oceanside Pier",
                Address = "Oceanside Pier, Oceanside, CA 92054, USA"
            };
            var MissionHillsHS = new Pin
            {
                Type = PinType.Place,
                Position = new Position(33.14482, -117.14321),
                Label = "Mission Hills High School",
                Address = "1 Mission Hills Ct, San Marcos, CA 92069, USA"
            };
            var SanMarcosLibrary = new Pin
            {
                Type = PinType.Place,
                Position = new Position(33.14124, -117.16053),
                Label = "San Marcos Library",
                Address = "2 Civic Center Dr, San Marcos, CA 92069, USA"
            };
            var MotoForza = new Pin
            {
                Type = PinType.Place,
                Position = new Position(33.12380, -117.10476),
                Label = "Moto Forza",
                Address = "1220 W Washington Ave, Escondido, CA 92029, USA"
            };

            Map.Pins.Add(PalomarHospital);
            Map.Pins.Add(EscondidoCycleCenter);
            Map.Pins.Add(OceansidePier);
            Map.Pins.Add(MissionHillsHS);
            Map.Pins.Add(SanMarcosLibrary);
            Map.Pins.Add(MotoForza);
        }


        //If a pin is selected, move to that pin location
        private void PinSelecter(object sender, EventArgs e)
        {
            pins.Title = "Pins";
            pins.TitleColor = Color.Blue;

            var item = (Picker)sender;
            int selected = item.SelectedIndex;

            if (selected == 0)
            {
                Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.12180, -117.12188), Distance.FromMiles(1)));
            }
            else if (selected == 1)
            {
                Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.12834, -117.10952), Distance.FromMiles(1)));
            }
            else if (selected == 2)
            {
                Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.19298, -117.38661), Distance.FromMiles(1)));
            }
            else if (selected == 3)
            {
                Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.14482, -117.14321), Distance.FromMiles(1)));
            }
            else if (selected == 4)
            {
                Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.14124, -117.16053), Distance.FromMiles(1)));
            }
            else if (selected == 5)
            {
                Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.12380, -117.10476), Distance.FromMiles(1)));
            }
            pins.Title = "Pins";
            pins.TitleColor = Color.Blue;
        }

        //Event handler if you click on the street tab
        private void Street_Clicked(object sender, EventArgs e)
        {
            Map.MapType = MapType.Street;
            Street_Button.BackgroundColor = Color.Blue;
            Satellite_Button.BackgroundColor = Color.White;
            Hybrid_Button.BackgroundColor = Color.White;
            Street_Button.TextColor = Color.White;
            Satellite_Button.TextColor = Color.Blue;
            Hybrid_Button.TextColor = Color.Blue;
        }

        //Event handler if you click on the satellite tab
        private void Satellite_Clicked(object sender, EventArgs e)
        {
            Map.MapType = MapType.Satellite;
            Street_Button.BackgroundColor = Color.White;
            Satellite_Button.BackgroundColor = Color.Blue;
            Hybrid_Button.BackgroundColor = Color.White;
            Satellite_Button.TextColor = Color.White;
            Street_Button.TextColor = Color.Blue;
            Hybrid_Button.TextColor = Color.Blue;
        }

        //Event handler if you click on the hybrid tab
        private void Hybrid_Clicked(object sender, EventArgs e)
        {
            Map.MapType = MapType.Hybrid;
            Street_Button.BackgroundColor = Color.White;
            Satellite_Button.BackgroundColor = Color.White;
            Hybrid_Button.BackgroundColor = Color.Blue;
            Hybrid_Button.TextColor = Color.White;
            Street_Button.TextColor = Color.Blue;
            Satellite_Button.TextColor = Color.Blue;
        }
    }

}
